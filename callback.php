<?php
define( "TIMESTAMPFORMAT",  "l jS F Y g:i:s a"); // Thursday 11th April 2019 2:01:49 pm
define( "MAILGUN_DOMAIN",   "mg.stackworks.co.uk" );
define( "MAILGUN_API_KEY",  "82f9b5953e380a192fa068060ceec9ca-8889127d-8209fff8" );

// define( "CALLBACK_TARGET",  "hockey.richard@gmail.com");
// define( "CALLBACK_CC",      "matt.klippel@stackworks.com");

define( "CALLBACK_TARGET",  "ben.stephens@stackworks.com");
define( "CALLBACK_CC",      "james.champ@stackworks.com, rachel.green@stackworks.com");

// set correct timezone for DST
date_default_timezone_set('Europe/London');

$http_host = $_SERVER['HTTP_HOST'];

switch( $http_host ) {
  case 'localhost' : {
    $_Base = '/Applications/MAMP/htdocs/stack/';
    $_Prefix = '/stack';
  } break;
  case 'production.stackworks.com' : {
    $_Base = '/var/www/html/stack-preview/';
    $_Prefix = '/stack-preview';
  } break;
  case 'stackworks.com' :
  case 'www.stackworks.com' : {
    $_Base = '/var/www/html/';
    $_Prefix = '';
  } break;
}
/*
nginx.conf url rewrite

/stack/callback/CAMPAIGN/USERID -> /stack/callback.php?campaign=CAMPAIGN&userid=USERID

# Stack callback
location /stack/callback/ {
	rewrite  ^/stack/callback/([0-9A-Za-z]+)/([0-9A-Za-z\-\_]+)(\/*)$ /stack/callback.php?campaign=$1&userid=$2;
}	

*/

function GUID()
{
    if (function_exists('com_create_guid') === true)
    {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

require "bootstrap.php";
// require 'vendor/autoload.php'; // overrides default autoload
use Mailgun\Mailgun;


// get user ID and campaign ID from URL
$campaign = $_GET['campaign'];
$userid = $_GET['userid'];

// set up HTML email template
$timestamp = date('l jS F Y g:i:s a');
// $email_template = file_get_contents(BASE.'callback_email_template.html');
$email_template = file_get_contents($_Base.'callback_email_plaintext.txt');
$thanks_template = file_get_contents($_Base.'callback_thanks.html');
$email = $email_template;
$thanks_page = $thanks_template;

$email_fields = [
  'id' => 'NULL',
  'name' => 'NAME',
  'company' => 'COMPANY',
  'email' => 'EMAIL',
  'phone' => 'PHONE',
  'timestamp' => 'NULL',
  'campaign' => 'NULL'
];

$thanks_fields = [
  'prefix' => $_Prefix,
  'message' => 'template message'
];

// set up values to place in template
// use $userid parameterd to get user details from DB SELECT * FROM user WHERE users.guid = $userid
$users = Users::where('guid',$userid)->first();
// $users = Users::where('guid','x')->first();

if ( gettype($users) !== 'NULL' ) {
  // user guid found in the Users database table
  $email_fields = [
    'id' => $userid,
    // 'name' => $users->name,
    'name' => $users->title . ' ' . $users->forename . ' ' . $users->surname,
    'job' => $users->job,
    'company' => $users->company,
    'email' => $users->email,
    'phone' => $users->phone,
    'timestamp' => $timestamp,
    'campaign' => $campaign
  ];


  // add entry to the 'clicks' database via Clicks model
  // check to see if userid/campaign click already exists in the db
  $clicks = Clicks::where([
    ['user',$userid],
    ['campaign',$campaign]
  ])->get();


  /*
  if ( count($clicks) > 0) {
    // user/campaign already in db
    $thanks_fields['message'] = 'existing click';

  } else {
  */
    // new user/campaing click
    // create new instance of Clicks model
    $click = new Clicks;

    // set properties of model
    $click->user = $userid;
    $click->campaign = $campaign;

    // save model to db
    $click->save();

    // send email
    // populate email template
    foreach($email_fields AS $key => $field) {
      $email = preg_replace ( '/\[\['.$key.'\]\]/', $field, $email);
    }

    // send email
    // https://documentation.mailgun.com/en/latest/api-sending.html#sending
    // $mg->messages()->send($domain, $params);
    // $mg = Mailgun::create(MAILGUN_API_KEY, 'https://api.eu.mailgun.net'); // For EU servers
    $mg = Mailgun::create(MAILGUN_API_KEY); // set up key with US server
    $mg->messages()->send(MAILGUN_DOMAIN, [
      'from'    => 'stack-callback@'.MAILGUN_DOMAIN,
      'to'      => CALLBACK_TARGET,
      'cc'    => CALLBACK_CC,
      'subject' => 'mailgun stack callback - '.$timestamp,
      'text'    => $email
      // 'html' => $email
    ]);

    // set thank you page message
    $thanks_fields['message'] = 'new click';
  /*
  }
  */

} else {
  // user guid not found
    $thanks_fields['message'] = 'no match';
}

// populate thank you page template
foreach($thanks_fields AS $key => $field) {
	$thanks_page = preg_replace ( '/\[\['.$key.'\]\]/', $field, $thanks_page);
}

// echo "<p>$_Base - $_Prefix</p>";

// echo $email;
echo $thanks_page;
?>
