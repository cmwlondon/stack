<?php
/* MODEL Users -> stack2019.users */
use Illuminate\Database\Eloquent\Model as Eloquent;

class Users extends Eloquent
{
   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
      'title', 'forename', 'surname', 'company', 'job', 'email', 'phone', 'postcode', 'website', 'guid'
   ];
   /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
   protected $hidden = [
       'remember_token',
   ];
}
