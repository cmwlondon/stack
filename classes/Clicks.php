<?php
/* MODEL Clicks -> stack2019.clicks */
use Illuminate\Database\Eloquent\Model as Eloquent;

/*
click
user -> users.guid
campaign
*/

class Clicks extends Eloquent
{
   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
       'user', 'campaign'
   ];

   /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
   protected $hidden = [
   ];

   public function getClicksLatestFirst( $query )
   {
      return $query->orderBy('created_at','ASC');
   }

   // one-to-one relationship: clicks to users 1 click -> 1 user
   // many-to-one: users to clicks 1 user -> N clicks
   // https://laravel.com/docs/5.1/eloquent-relationships
   // clicks LEFT JOIN users ON clicks.user = users.guid
   public function User()
   {
      return $this->hasOne('Users','guid','user');
   }
 }