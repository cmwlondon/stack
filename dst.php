<?php
// set correct timezone
date_default_timezone_set('Europe/London');

if (date('I', time()))
{
	echo '<p>We\'re in DST!</p>';
}
else
{
	echo '<p>We\'re not in DST!</p>';
}

echo "<p>".ini_get('date.timezone')."</p>";
echo "<p>".date_default_timezone_get()."</p>";
// Europe/London
$timestamp = date('l jS F Y g:i:s a');
echo "<p>".$timestamp."</p>";
?>