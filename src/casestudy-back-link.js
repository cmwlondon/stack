$(document).ready(function () {
	/* ---------------------------------------------------------------------------- */
	// casetudy
	// dependent: /casetudy/*.html

	if ( $('body').hasClass('casestudy') ) {

		/*
		amend back 
		default -> ../work.html 'BACK TO OUR WORK'
		work.html -> ../work.html 'BACK TO OUR WORK'
		consumer.html -> ../consumer.html 'BACK TO CONSUMER STACK'
		business.html -> ../business.html 'BACK TO BUSINESS STACK'
		(work|consumer|business)\.html
		*/

		/*
	      <div class="buttonBox">
	        <a href="../work.html" id="casestudyBackLink">BACK TO OUR WORK</a>
	      </div>
		*/
		let referrer = document.referrer;
		let backLink = "work.html";
		let backText = "BACK TO OUR WORK";
		let referrerRegEx = new RegExp('(consumer|business)\.html', '');
		let result = referrerRegEx.exec(referrer);

		if(result != null){
			backLink = result[0];
			backText = (backLink === 'business.html') ? 'BACK TO BUSINESS STACK' : 'BACK TO CONSUMER STACK';
		}
		$('#casestudyBackLink').attr({ "href" : "../" + backLink }).text(backText);
	}
}
