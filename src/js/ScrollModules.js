/* ------------------------------------------------ */
// ScrollModules
/* ------------------------------------------------ */

function ScrollModules(parameters) {
	this.parameters = parameters;

	this.container = parameters.container;
	this.modules = parameters.modules;
	this.positionDisplay = parameters.positionDisplay;
	this.logoDisplay = parameters.logoDisplay;

	this.moduleOffsets = [];
	this.currentModule = 0;

	this.ignoreScrollEvent = false;

	this.urlwatcher;
	this.urlHash;

	this.init();
}

ScrollModules.prototype = {
	"constructor" : ScrollModules,
	"template" : function () {var that = this; },
	
	"init" : function () {
		var that = this;

		this.index();

		// set scroll event
		this.bindScrollEvent();

		/*
		// check for module anchor in URL
		this.urlHash = this.checkURL();
		if ( this.urlHash != '') {
			this.findModule(this.urlHash);
		}

		$(window).bind("hashchange", function(){
		    // Anchor has changed.
		    var newHash = that.checkURL();
		    this.urlHash = newHash;
			that.findModule(newHash);
		});
		*/

		// handle position display
		this.positionDisplay.on('click',function(e){
			e.preventDefault();
			that.jumpToModule( that.positionDisplay.index($(this)) );
		});

		$('.scrollDown').on('click', function(e){
			e.preventDefault();
			that.jumpToModule(1);
		});

		// set window size change event
		$(window).smartresize(function(){
			windowSize = {
				"w" : $(window).width(),
				"h" : $(window).height(),
				"a" : $(window).width() / $(window).height()
			};

			mobileWidth = (windowSize.w < 1024) && (windowSize.a < 1);

			if (windowSize.a > 1) {
				$('html').removeClass('portraitAspectRatio');
				$('html').addClass('landscapeAspectRatio');
			} else {
				$('html').removeClass('landscapeAspectRatio');
				$('html').addClass('portraitAspectRatio');
			}

			// get updated sizes/positions of modules	
			that.modules.each(function(i){
				that.index();
				that.scrollUpdate();
			});

			/* conection to ParallaxScroll instance 'thisParallaxScroll' START */
			// resize parallax images to fit new window dimensions + switch between desktop/mobile images

			thisParallaxScroll.modules = that.moduleOffsets; // pass updated sizes/positions of modules into thisParallaxScroll
			thisParallaxScroll.resizer( $(window).scrollTop() ); // resize mirrors & images

			/* END */
		});

	},

	/* --------------------------------------------------------------------------- */

	"checkURL" : function () {
		var that = this;

		return location.hash.replace('#','');
	},

	/* --------------------------------------------------------------------------- */

	"index" : function () {

		var that = this,
			index,
			dataLoop,
			thisModuleOffset,
			moduleColour,
			moduleWidth,
			moduleID,
			moduleAlign,
			moduleDesktopImage,
			moduleMobileImage,
			moduleTheta,
			moduleTop = 0;

		this.moduleOffsets = [];
		
		this.modules.each(function(i){
			thisModuleOffset = $(this).position();

			if ( i > 0 ) {

				that.moduleOffsets.push({
					"index" : index,
					"id" : moduleID,
					"min" : moduleTop,
					"max" : thisModuleOffset.top,
					"middle" : Math.floor((thisModuleOffset.top - moduleTop) / 2) + moduleTop,
					"width" : moduleWidth,
					"height" : thisModuleOffset.top - moduleTop,
					"colour" : moduleColour
				});

			}

			index = i;
			moduleWidth = $(this).width();
			moduleID = $(this).attr('id');
			moduleColour = $(this).attr('data-logo-color');

			if (thisModuleOffset.top > moduleTop) {
				moduleTop = thisModuleOffset.top;
			}
		});

		// find offset of work block, marking end of parallax elemnts
		thisModuleOffset = $('.work').position();

		this.moduleOffsets.push({
			"index" : index,
			"id" : moduleID,
			"min" : moduleTop,
			"max" : thisModuleOffset.top,
			"height" : thisModuleOffset.top - moduleTop,
			"width" : moduleWidth,
			"middle" : Math.floor((thisModuleOffset.top - moduleTop) / 2) + moduleTop,
			"colour" : moduleColour
		});


		this.moduleOffsets.map(function(module){
			$('#' + module.id).attr({
				"data-min" : module.min,
				"data-max" : module.max,
				"data-middle" : module.middle,
				"data-height" : module.height
			});
		});

		/*
		this.moduleOffsets.map(function(module){
			console.log(module);
		});
		*/
	},

	/* --------------------------------------------------------------------------- */

	"bindScrollEvent" : function () {
		var that = this;

		$(window).scroll(function(){
			if ( !that.container.hasClass('ignoreScroll') ) {
				var throttle = window.setTimeout(function(){
					that.scrollUpdate();
				}, 50);
			}
		});
	},

	/* --------------------------------------------------------------------------- */

	"unbindScrollEvent" : function () {
		var that = this;
	},

	/* --------------------------------------------------------------------------- */

	"findModule" : function (hash) {
		var that = this;

		this.moduleOffsets.map(function (module,index) {
			if (module.id == hash ) {
				that.jumpToModule(index);
			}
		});

	},

	/* --------------------------------------------------------------------------- */

	"setModuleState" : function (modIndex) {
		var that = this;
		// set state of right hand position indicator
		this.modules.eq(this.currentModule).removeClass('on');
		this.modules.eq(modIndex).addClass('on');

	},

	/* --------------------------------------------------------------------------- */

	"setPosition" : function (modIndex) {
		var that = this;
		// set state of right hand position indicator
		this.positionDisplay.eq(this.currentModule).removeClass('on');
		this.positionDisplay.eq(modIndex).addClass('on');
	},

	/* --------------------------------------------------------------------------- */

	"setPositionDisplayState" : function (showControl) {
		var that = this;

		if ( showControl ) {
			$('.position').removeClass('hidep');
		} else {
			$('.position').addClass('hidep');
		}
	},

	/* --------------------------------------------------------------------------- */

	"setLogoColour" : function (colour) {
		var that = this;

		// this.logoDisplay.css({ "fill" : colour });
		return true;
	},

	/* --------------------------------------------------------------------------- */

	"setLogoSurround" : function (enabled) {
		var that = this;

		/*
		if ( enabled ) {
			$('a.home').removeClass('nobg');
		} else {
			$('a.home').addClass('nobg');
		}
		*/
		return true;
	},

	/* --------------------------------------------------------------------------- */

	"jumpToModule" : function (moduleIndex) {
		var that = this;
		// jump to module when user clicks on position indicator

		this.ignoreScrollEvent = true;
		this.container.addClass('ignoreScroll');

		// $(window).scrollTop( moduleOffsets[moduleIndex].min );
		$("html, body").animate({ "scrollTop" : this.moduleOffsets[moduleIndex].min + "px" }, 300, function(){
			that.setPosition(moduleIndex);
			that.setLogoColour( that.modules.eq(moduleIndex).attr('data-logo-color') );
			that.currentModule = moduleIndex;

    		// show/hide transparent block around logo
    		that.setLogoSurround( moduleIndex !== 0 );

			// location.hash = that.moduleOffsets[moduleIndex].id;

			var throttle = window.setTimeout(function(){
				that.ignoreScrollEvent = false;
				that.container.removeClass('ignoreScroll');
			}, 150);
		});
	},

	/* --------------------------------------------------------------------------- */

	"scrollUpdate" : function () {
		var that = this;

		// handle scroll events, update logo colour & position indicator
	    var scrollPos = $(window).scrollTop(),
	    	modIndex = 0,
	    	modCount = this.moduleOffsets.length,
	    	thisModule,
	    	thisModuleOffset,
	    	pastLastMod = true;

	    if (modCount > 0 && !this.ignoreScrollEvent) {
		    do {
		    	thisModule = this.modules.eq(modIndex);
		    	thisModuleOffset = this.moduleOffsets[modIndex];

		    	if (scrollPos >= thisModuleOffset.min && scrollPos <= thisModuleOffset.max) {

		    		// show/hide transparent block around logo
					this.setLogoSurround( modIndex !== 0 );

		    		pastLastMod = false;
		    		// show position control
		    		this.setPositionDisplayState(true);

		    		if (this.currentModule != modIndex) {
		    			// toggle module states
		    			this.setModuleState(modIndex);

		    			// update dot navigation
		    			this.setPosition(modIndex);

		    			// update stacklogo fill colour
		    			// this.setLogoColour( thisModule.attr('data-logo-color') );
		    			this.setLogoColour( thisModuleOffset.colour );

		    			this.currentModule = modIndex;
		    		}

		    		break;
		    	}
				modIndex++;			    	
		    } while (modIndex < modCount)

		    // check to see if scrolltop lien within last module
	    	thisModuleOffset = this.moduleOffsets[ (modCount - 1) ];
		    if (scrollPos >= thisModuleOffset.min && scrollPos <= thisModuleOffset.max) {
		    	this.setLogoColour( thisModuleOffset.colour );
		    }

		    // hide controls when bottom of last module reaches half way up browser viewport
		    if ( scrollPos > (thisModuleOffset.min + (thisModuleOffset.height / 2)) ) {
		    	// reset logo colour to white
		    	this.setLogoColour( '#fff' );
		    	// hide position control
	    		this.setPositionDisplayState(false);
		    }
	    }
	},

	/* --------------------------------------------------------------------------- */

	"windowUpdate" : function () {
		var that = this;
		// re-index module scroll offsets
	},

	/* --------------------------------------------------------------------------- */

	"pastBottomOfContainer" : function (point) {
		var that = this;
		// reset logo colour and hide position display if the user has scrolled down past the bottom of the module container
	}

	/* --------------------------------------------------------------------------- */

}

// window.ScrollModules = ScrollModules;
export default ScrollModules;
