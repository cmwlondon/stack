
	/* ------------------------------------------------ */
	// Utility
	/* ------------------------------------------------ */

	function Utility(parameters) {
		this.parameters = parameters;

	}
	
	Utility.prototype = {
		"constructor" : Utility,

		"template" : function () { var that = this; },

	    "cloneArray" : function (array) {
	      var that = this,
	          ai = 0,
	          ac = array.length,
	          ta,
	          newArray = [],
	          newObject;

	      do {
	        ta = array[ai];

	        newObject = {};

	        Object.keys(ta).map(function(property){
	        	newObject[property] = ta[property];
	        });
	        newArray.push(newObject);

			/* */

	        ai++;
	      } while (ai < ac)

	      return newArray;
	    },

		/* --------------------------------------------------------------------------- */

		"quickSort" : function  (arr, left, right) {
			var i = left,
				j = right,
				tmp,
				pivotidx = (left + right) / 2,
				pivot = arr[pivotidx.toFixed()];  

			/* partition */
			while (i <= j)
			{
				// ascending order
				while (arr[i] < pivot) {
					i++;
				}

				while (arr[j] > pivot) {
					j--;
				}

				// descending order
				/*
				while (arr[i] > pivot) {
					i++;
				}

				while (arr[j] < pivot) {
					j--;
				}
				*/

				if (i <= j)
				{
					tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;

					i++;
					j--;
				}
			}

			/* recursion */
			if (left < j)
				this.quickSort(arr, left, j);
			if (i < right)
				this.quickSort(arr, i, right);
			return arr;
	    }

	    /* ---------------------------------------------------------------------------------------------------------------- */

	}
	// window.Utility = Utility;
	export default Utility;
