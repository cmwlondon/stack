/* ------------------------------------------------ */
// PanelBackgroundVideo
/* ------------------------------------------------ */

function PanelBackgroundVideo(parameters) {
	this.parameters = parameters;

	this.panel = parameters.panel;
	this.videoSource = parameters.videoSource;

	this.isLoaded = false;
	this.isVisible = false;
	this.isPlaying = false;


	this.init();
}

PanelBackgroundVideo.prototype = {
	"constructor" : PanelBackgroundVideo,
	"template" : function () {let that = this; },
	
	"init" : function () {
		let that = this;
	},

	"insertVideo" : function() {
		let that = this;

		// let newVideoTag = $('<video preload="metadata" nocontrols autoplay playsinline muted loop></video>');  // muted needs to be in the tag when it is generated
		let newVideoTag = $('<video preload="metadata" nocontrols playsinline muted autoplay loop></video>');  // muted needs to be in the tag when it is generated
		newVideoTag
		.attr({
			"id" : "panel1"
		});

		// attach mp4 source
		if ( this.videoSource.mp4 ) {
			let newVideoSourceMP4 = $('<source></source>').attr({
				"type" : "video/mp4",
				"src" : this.videoSource.mp4
			});
			newVideoTag.append(newVideoSourceMP4);
		}

		newVideoTag.on('loadedmetadata', (event) => {
			that.isLoaded = true;
			let video = $(event.currentTarget).get(0),
			readyInfo = video.readyState;
			video.play();

			return true;
		});

		// loop video when it ends
		newVideoTag.on('ended', (event) => {
			let video = $(event.currentTarget).get(0);
			video.currentTime = 0;
			video.play();

			return true;
		});

		this.videoTag = newVideoTag;
		this.panel.append(newVideoTag);
	},

	"startVideo" : function() {
		let that = this;

		if (this.isLoaded) {
			let video = this.videoTag.get(0);
			video.play();

			this.setIsPlaying(true);
		}
	},
	"stopVideo" : function() {
		let that = this;

		if (this.isLoaded) {
			let video = this.videoTag.get(0);
			video.pause();

			this.setIsPlaying(false);
		}
	},
	"setIsVisible" : function(isVisible) {
		this.isVisible = isVisible;
	},
	"getIsVisible" : function() {
		return this.isVisible;
	},
	"setIsPlaying" : function(isPlaying) {
		this.isPlaying = isPlaying;
	},
	"getIsPlaying" : function() {
		return this.isPlaying;
	}
}

export default PanelBackgroundVideo;