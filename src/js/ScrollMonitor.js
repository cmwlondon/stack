import QueueSingle from './QueueSingle.js'
import PanelBackgroundVideo from './PanelBackgroundVideo.js'

/* ------------------------------------------------ */
// ScrollMonitor
/* ------------------------------------------------ */

function ScrollMonitor(parameters) {
	this.parameters = parameters;

	this.dotsSelector = parameters.dotsSelector;
	this.moduleClass = parameters.moduleClass;

	this.init();
}

ScrollMonitor.prototype = {
	"constructor" : ScrollMonitor,
	"template" : function () {let that = this; },
	
	"init" : function () {
		let that = this;

		this.dots = $(this.dotsSelector);
		this.modules = $(this.moduleClass);
		this.moduleCount = this.modules.length;
		this.currentModule = 0;
		this.currentModuleType ='';
		that.currentScrollTop = 0;
		this.moduleIndex = [];
		this.bgpanels = [];

		this.singlePanelbg = null;
		this.singlePanelbgIndexed = false;

		this.indexModules();
		this.bindScroll();
	},
	"indexModules" : function () {
		let that = this;

		// class="" (text|picture|work|capabilities)
		let moduleTypesRegex = RegExp('(videoModule|text|rule|work|capabilities)');

		let rules = null;
		let rulesOffset = 0;
		let rulesTop = 0;

		// get scroll offset of each module
		this.modules.each((i,el) => {
			let module = $(el);
			let rt = moduleTypesRegex.exec(module.attr('class'));
			let moduleType = (rt) ? rt[1] : "";
			let moduleID = module.attr('id');

			let moduleOffset = module.position();
			let height = module.height();
			let top = moduleOffset.top;
			let bottom = top + module.height();

			/* fade in work module
			if( moduleType === 'work' ) {
				that.workNode = $(this);
				that.workTrigger = {
					"top" : moduleOffset.top - (windowSize.h / 2)
				};
			}
			*/

			if( moduleType === 'rule' ) {
				// get rules container top offset
				if (!this.singlePanelbgIndexed) {
					rules = $('section.rules');
					rulesOffset = rules.position();
					rulesTop = rulesOffset.top;

					this.singlePanelbgIndexed = true;
				}

				// account for rules modules sitting inside container
				top = moduleOffset.top + rulesTop;
				bottom = top + module.height();
			}

			if( moduleType === 'videoModule' ) {
				that.videoBottom = bottom;
			}

			// console.log("type: %s id: %s top: ", moduleType, moduleID, top);

			let moduleData = {
				"index" : i,
				"id" : moduleID,
				"type" : moduleType,
				"top" : top,
				"bottom" : bottom,
				"height" : height
			}

			that.moduleIndex.push(moduleData);
		});

		this.moduleCount = this.moduleIndex.length;
		this.lastModule = this.moduleCount - 1;
		this.maxHeight = this.moduleIndex[this.lastModule].top + this.moduleIndex[this.lastModule].height;

		// add dummy module entry to mark end of page
		let footerBox = $('footer').eq(0);
		let footerBoxPosition = footerBox.position();

		that.moduleIndex.push({
			"index" : this.moduleCount,
			"id" : "footer",
			"type" : "footer",
			"top" : footerBoxPosition.top,
			"bottom" : footerBoxPosition.top + footerBox.height(),
			"height" : footerBox.height()
		});

		this.currentModule = 0;
		this.currentModuleType = this.moduleIndex[this.currentModule].type
		this.currentModuleNode = $('#' + this.moduleIndex[this.currentModule].id);

	},
	"bindScroll" : function () {
		let that = this;

		$(window).scroll(function(){
			// let throttle = window.setTimeout(function(){

				that.scrollHandler();
			// }, 10);
		});
	},
	"scrollHandler" : function () {
		let that = this;

		let currentScrollTop = $(window).scrollTop();

		let nModule = {};
		let visibleItems = [];
		let notVisibleItems = [];

		for(var n = 0; n < this.moduleIndex.length; n++) {
			nModule = this.moduleIndex[n];

			// find visible items
			if( ( nModule.top < (currentScrollTop + windowSize.h) ) && ( nModule.bottom > currentScrollTop ) ) {
				this.dots.eq(n).addClass('on');
				visibleItems.push({
					"index" : n,
					"type" : nModule.type,
					"id" : nModule.id,
					"top" : nModule.top,
					"top2" : (nModule.top - windowSize.h),
					"bottom" : nModule.bottom ,
					"range" : nModule.bottom - (nModule.top - windowSize.h),
					"hasVideoBackground" : nModule.hasPanelVideo,
					"videoBackgroundIndex" : ((nModule.hasPanelVideo) ? nModule.panelVideo : -1 )
				});
			} else {
				// handle items moving out of visibility

				notVisibleItems.push({
					"id" : nModule.id
				});

				this.dots.eq(n).removeClass('on');
			}
		}

		window.requestAnimationFrame( function(x){

			if (currentScrollTop > that.videoBottom) {
				$('.logoAnimation').addClass('go');
			} else {
				$('.logoAnimation').removeClass('go');
			}

			notVisibleItems.map(function(item){
				$( '#' + item.id ).removeClass('active ta hold');

				//check for video backgrouns moving out of view
			},that);		

			visibleItems.map(function(item){
				let itemNode = $( '#' + item.id );

				let percent = (currentScrollTop - item.top2)  / item.range;
				/*
				let p1 = 0;
				// modify curve 33% - 66%
				if (percent > 0.25 && percent < 0.66) {
					p1 = ((percent - 0.25) * 3);
				} else if(percent > 0.66){
					p1 = 1;
				}
				*/
				switch(item.type){
					case "text" : {
						itemNode.addClass('active');						

						if( ( item.top <= currentScrollTop ) ){
							itemNode.addClass('ta');
						}

						// detect when current item begins to scroll off of top of browser window
						if( (item.top + windowSize.h) <= currentScrollTop ){
							itemNode.removeClass('ta');
						}

					} break;

					case "rule" : {
						itemNode.addClass('active');						

						if( ( item.top <= currentScrollTop ) ){
							itemNode.addClass('ta');
						}

						if( (item.top + windowSize.h) <= currentScrollTop ){
							itemNode.removeClass('ta');
						}

					} break;

					case "work" : {
						if( ( item.top < currentScrollTop ) && !itemNode.hasClass('active') ){
							itemNode.addClass('active');						
						}
					} break;
					/*
					case "videoModule" : {} break;
					case "work" : {} break;
					case "capabilities" : {} break;
					case "footer" : {} break;
					*/
					default : {} break;
				}
			},that);
		});
	}

	/* --------------------------------------------------------------------------- */

}

window.ScrollMonitor = ScrollMonitor;
export default ScrollMonitor;