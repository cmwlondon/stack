<?php
/*
list clicks and associated users
*/

require "bootstrap.php";
// require 'vendor/autoload.php'; // overrrides default autoload

// set correct timezone for DST
date_default_timezone_set('Europe/London');

$http_host = $_SERVER['HTTP_HOST'];

switch( $http_host ) {
  case 'localhost' : {
    $_Base = '/Applications/MAMP/htdocs/stack/';
    $_Prefix = '/stack';
  } break;
  case 'production.stackworks.com' : {
    $_Base = '/var/www/html/stack-preview/';
    $_Prefix = '/stack-preview';
  } break;
  case 'stackworks.com' :
  case 'www.stackworks.com' : {
    $_Base = '/var/www/html/';
    $_Prefix = '';
  } break;
}

$timestamp = date('l jS F Y g:i:s a');

// set up HTML template
$clicks_page_template = file_get_contents($_Base.'callback_clicks.html');
$clicks_page = $clicks_page_template;

/*
clicks -> hasOne( users )

SQL query:
  SELECT
   users.title,
   users.forename,
   users.surname,
   users.job,
   users.email,
   users.company,
   clicks.campaign,
   clicks.created_at
  FROM
   clicks
  LEFT JOIN
   users
  ON
   clicks.user = users.guid
  ORDER BY
   clicks.created_at DESC
*/

/*
users -> hasMany( clicks )
SELECT
*
FROM 
  users 
LEFT JOIN
 clicks
ON
 users.guid = clicks.user 
WHERE
NOT
 ( clicks.user is null )
GROUP BY
 users.id
 */

$output = '<ul class="clicks">';
$clicks = Clicks::orderBy('created_at', 'DESC')->get();

if ( count($clicks) > 0 ) {
  foreach($clicks as $click) {
    $output = $output."<li><p><strong>".$click->User->title." ".$click->User->forename." ".$click->User->surname."</strong></p><p>email: <strong>".$click->User->email."</strong></p><p>job: <strong>".$click->User->job."</strong></p><p>company: <strong>".$click->User->company."</strong></p><p>".$click->campaign." // ".$click->created_at."</p></li>";
  }
  $output = $output."</ul>";
}

// populate email template
$fields = [
  'clicks' => $output,
  'prefix' => $_Prefix
];
foreach($fields AS $key => $field) {
  $clicks_page = preg_replace ( '/\[\['.$key.'\]\]/', $field, $clicks_page);
}


// echo clicks page;
echo $clicks_page;

?>
