<?php
// stack2019.clicks

/*
id unsigned int, auto incerement, primary key
user text foreign key -> users.guid
campaign text
timestamp
*/

require "../bootstrap.php";
use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('clicks', function ($table) {
   $table->increments('id');
   $table->string('user');
   $table->string('campaign');
   $table->timestamps();
});
// created_at
// updated_at

?>