<?php
// stack2019.users

/*
id unsigned int, auto incerement, primary key
name text
company text
email text
phone text
guid text
*/

require "../bootstrap.php";
use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('users', function ($table) {
   $table->increments('id');
   $table->string('title');
   $table->string('forename');
   $table->string('surname');
   $table->string('job');
   $table->string('email')->unique();
   $table->string('phone');
   $table->string('company');
   $table->string('postcode');
   $table->string('website');
   $table->string('guid')->unique();
   $table->timestamps();
});
// created_at
// updated_at
?>